<?php
/**
 * @file
 * Admin page
 */

/**
 * Page callback for admin/forum/forums
 */
function admin_forums_page_callback() {
    $output = '';
    $header = array(t('Forum ID'), t('User ID'), t('Name'), t('Description'), t('Opties'));

    if ($entries = osforum_load_forums()) {
        $rows = array();
        foreach ($entries as $entry) {
            // Sanitize the data before handing it off to the theme layer.
            $entry->link = "<a href='forums/manage/".$entry->fid."'>Edit</a> / <a href='tables/delete/".$entry->fid."'>Delete</a>";
            $rows[] = (array) $entry;
        }

        // Make a table for them.
        $output .= theme('table', array('header' => $header, 'rows' => $rows));
    }
    else {
        $output .= theme('table', array('header' => $header, 'rows' => array()));
        drupal_set_message(t('None forums are created yet.'), 'warning');
    }

    return $output;
}

/**
 * Page callback for admin/forum/forums/manage/%
 */
function osforum_forum_manage($fid) {
    $count = db_select('osforum_forums', 'f')
        ->fields('f')
        ->condition('fid', arg(4), '=')
        ->execute()
        ->rowCount();

    if ($count < 1) {
        drupal_set_message(t('Forum ID: %fid does not exsits.', array('%fid' => $fid)), 'warning');
        drupal_goto('admin/forum/forums');
    }

    return drupal_get_form('osforum_manage_forum_form');
}

/**
 * Page callback for admin/forum/forums/delete/%
 */
function osforum_forum_delete($fid) {
    db_delete('osforum_forums')
        ->condition('fid', $fid, '=')
        ->execute();

    drupal_set_message(t('Forum deleted with success!'), 'status');
    drupal_goto('admin/forum/forums', array('fragment' => 'nl/admin/overlay'));
}

/**
 * Page callback for admin/forum/forums/create
 */
function osforum_forum_create() {
    return drupal_get_form('osforum_create_forum_form');
}


/**
 * Function that returns all forums with their values
 */
function osforum_load_forums($entry = array()) {
    // Read all fields from the osforums_forums table.
    $select = db_select('osforum_forums', 'f');
    $select->fields('f');

    // Add each field and value as a condition to this query.
    foreach ($entry as $field => $value) {
        $select->condition($field, $value);
    }
    // Return the result in object format.
    return $select->execute()->fetchAll();
}

/**
 * Create osforum_manage_forum_form form
 */
function osforum_manage_forum_form() {
    $form = array();

    $forum = db_select('osforum_forums', 'f')
        ->fields('f')
        ->condition('fid', arg(4), '=')
        ->execute()
        ->fetchObject();

    // Forum name textfield
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#description' => t('Name of the Forum visible for anyone.'),
        '#default_value' => $forum->name,
        '#size' => 60,
        '#maxlength' => 40,
        '#required' => TRUE,
    );

    // Forum description textfield
    $form['desc'] = array(
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#description' => t('The Forum description.'),
        '#default_value' => $forum->description,
        '#size' => 60,
        '#maxlength' => 100,
        '#required' => TRUE,
    );

    $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update!'),
    );

    return $form;
}

/**
 * Form submit handler for osforum_manage_forum_form form
 */
function osforum_manage_forum_form_submit(&$form, &$form_state) {

    $fields = array(
        'name' => $form_state['values']['name'],
        'description' => $form_state['values']['desc']
    );

    try {
        // db_update()...->execute() returns the number of rows updated.
        $count = db_update('osforum_forums')
            ->fields($fields)
            ->condition('fid', arg(4), '=')
            ->execute();

        drupal_set_message(t('%name updated with success!', array('%name' => $form_state['values']['name'])), 'status');
    }
    catch (Exception $e) {
        drupal_set_message(t('db_update failed. Message = %message, query= %query',
            array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    }

}

/**
 * Create osforum_create_forum_form form
 */
function osforum_create_forum_form() {
    $form = array();

    // Forum name textfield
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#description' => t('Name of the Forum visible for anyone.'),
        '#size' => 60,
        '#maxlength' => 40,
        '#required' => TRUE,
    );

    // Forum description textfield
    $form['desc'] = array(
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#description' => t('The Forum description.'),
        '#size' => 60,
        '#maxlength' => 100,
        '#required' => TRUE,
    );

    $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Create Forum!'),
    );

    return $form;
}

/**
 * Form submit handler for osforum_create_forum_form form
 */
function osforum_create_forum_form_submit(&$form, &$form_state) {
    global $user;

    $forum = db_select('osforum_forums', 'f')
        ->fields('f')
        ->condition('name', $form_state['values']['name'], '=')
        ->execute()
        ->rowCount();

    if ($forum != 0) {
        drupal_set_message(t('Forum: %name already exsits.', array('%name' => $form_state['values']['name'])), 'warning');
    } else {

        $fields = array(
            'name' => $form_state['values']['name'],
            'description' => $form_state['values']['desc'],
            'uid' => $user->uid
        );

        try {
            // db_update()...->execute() returns the number of rows updated.
            $count = db_insert('osforum_forums')
                ->fields($fields)
                ->execute();

            drupal_set_message(t('%name created with success!', array('%name' => $form_state['values']['name'])), 'status');
        }
        catch (Exception $e) {
            drupal_set_message(t('db_create failed. Message = %message, query= %query',
                array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
        }
    }
}